FROM alpine:3.7

LABEL maintainer="kebabra.abdessamad@outlook.com"

RUN apk add --update nodejs nodejs-npm

COPY . /src

WORKDIR /src

EXPOSE 8081

ENTRYPOINT [ "node", "./index.js" ]